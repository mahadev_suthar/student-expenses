<?php
$activePage = basename($_SERVER['PHP_SELF'], ".php");
?>
<div class="box box-element ui-draggable" style="display: block;">
    <div class="view">
        <ul class="nav nav-tabs">
            <li class="<?php if($activePage == 'dashboard') echo 'active' ?>"><a href="dashboard.php">Dashboard</a></li>
            <li class="dropdown <?php if($activePage == 'add_expense' || $activePage == 'all_expenses') echo 'active' ?>">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false">Expenses<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="add_expense.php">Add New</a></li>
                    <li><a href="all_expenses.php">View All</a></li>
                </ul>
            </li>
            <li class="<?php if($activePage == 'add_balance') echo 'active' ?>"><a href="add_balance.php">Add Balance</a></li>
            <li class="dropdown <?php if($activePage == 'add_user' || $activePage == 'users') echo 'active' ?>">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false">Users<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="add_user.php">Add New</a></li>
                    <li><a href="users.php">View All</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div><br><br>