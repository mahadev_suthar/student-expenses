<?php
session_start();
if (!isset($_SESSION['user']) || empty($_SESSION['user'])) {
    header("location: index.php");
    die;
}
include 'includes/inc_db_con.php';
if(isset($_GET['id']) && !empty($_GET['id'])){
$user_data = Query("Select * from `users` where user_id=".$_GET['id']);   
$user_data = GetAssoc($user_data);
$user_rec = Query("Select * from `records` where user_id=".$_GET['id']);
}else{
    header("location: dashboard.php");
    die;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Student Expenses</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
    </head>

    <body>
        <div class="container">
            <h3 style="text-align: center">Today is <?= date("d-m-Y"); ?></h3><br>
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <h1 style="float: left"><?= $user_data['fullname']?></h1>
                    <h1 style="float: right">Amount Paid: <?= $user_data['amount_paid']?></h1><br><br><br><br><br><br><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Amount</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            $remaining = 0;
                            while ($record = GetAssoc($user_rec)) {
                                $remaining += $record['amount'];
                                if ($i % 2 == "0")
                                    $class = "class='active'";
                                else
                                    $class = "class='success'";
                                ?>
                                <tr <?= $class ?>>
                                    <td><?= $i ?></td>
                                    <td><?= $record['amount'] ?></td>
                                    <td><?= $record['exp_date'] ?></td>
                                </tr>
                                <?php $i++; 
                            }$final = $user_data['amount_paid']-$remaining;?>
                        </tbody>
                    </table>
                    <div id="remaining">
                        Remaining Balance : <b><?= $final ?></b>
                        </div>
                </div>
            </div>
        </div>
    </body>
</html>
