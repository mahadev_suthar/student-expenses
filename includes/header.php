<?php
session_start();
if (!isset($_SESSION['user']) || empty($_SESSION['user'])) {
    header("location: index.php");
    die;
}
include 'inc_db_con.php';
$grand_total = 0;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Student Expenses</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    </head>

    <body>
        <div class="container">
            <h3 style="text-align: center">Today is <?= date("d-m-Y"); ?></h3><br>
            <h3 style="float: right;"><a href="process.php?logout=1" style="text-decoration: none;">Logout</a></h3><br>
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <h1 style="text-align: center;">Student Expenses Record</h1><br><br><br>
                    <?php include_once 'menu.php'; ?>