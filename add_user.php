<?php
include_once 'includes/header.php';
?>
<h1>Add Balance</h1><br>
<form action="process.php" class="form-horizontal" method="post">
    <div class="form-group">
        <label for="fullname" class="col-sm-2 control-label">Fullname</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="fullname" id="fullname" required="">
        </div>
    </div><br>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-4">
            <input type="email" class="form-control" name="email" id="email" required="">
        </div>
    </div><br>
    
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Starting Balance</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="balance" id="balance" value="0" required="">
        </div>
    </div><br>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="add_user" class="btn btn-default">Submit</button>
        </div>
    </div>
</form>
</div>
</div>
</div>
</body>
</html>
