<?php
include_once 'includes/header.php';
if (isset($_GET['success']) && $_GET['success'] == "1") {
    echo "<p style='color:green; margin-left:196px;'>Record Successfully Inserted</p>";
}
$users = Query("Select * from users where status='active'");
?>
<h1>Add Expense</h1><br/>
<form action="process.php" class="form-horizontal" method="post">
    <div class="form-group">
        <label for="amount" class="col-sm-2 control-label">Enter Amount</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="amount" id="amount" required="">
        </div>
    </div><br>

    <div class="form-group">
        <label for="details" class="col-sm-2 control-label">Enter Details</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="details" id="details" required="">
        </div>
    </div><br>
    
    <div class="form-group">
        <label for="details" class="col-sm-2 control-label">Date</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="detail_date" required="" value="<?= date('Y-m-d') ?>">
        </div>
    </div><br>

    <div class="form-group">
        <label for="divide" class="col-sm-2 control-label">Select Members</label>
        <div class="col-sm-10">
            <input type="checkbox"  name="check_all" id="checkAll"> Check All<br><br>
            <?php
            while ($user = GetAssoc($users)) {
                $grand_total+=$user['amount_paid'];
                ?>
                <input type="checkbox" class="persons"  name="persons[]" value="<?= $user['user_id'] ?>"><?= " " . $user['fullname'] ?><br><br>
            <?php } ?>
        </div>
    </div><br>


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="add_exp" class="btn btn-default">Submit</button>
        </div>
    </div>
</form>
<br><h1>Latest Expenses</h1><br>
<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        $tot_daily = 0;
        $daily = Query("Select * from `daily` order by daily_date desc limit 0,5");
        while ($rec = GetAssoc($daily)) {
            if ($i % 2 == "0")
                $class = "class='active'";
            else
                $class = "class='success'";
            $tot_daily+=$rec['daily_amount'];
            ?>
            <tr <?= $class ?>>
                <td><?= $i ?></td>
                <td><?= $rec['daily_date'] ?></td>
                <td><?= $rec['daily_amount'] ?></td>
                <td><?= $rec['daily_details'] ?></td>
            </tr>
            <?php
            $i++;
        }
        ?>

    </tbody>
</table>
<div class="daily_rec">
    <h4>Total Expended : <?= $tot_daily ?></h4>
    <h4>Total Remains  : <?= $grand_total - $tot_daily ?></h4>
</div>
</div>
</div>
</div>
</body>
</html>
<script>
    $(document).ready(function(){
        $("#checkAll").change(function(){
            
            if($(this).is(':checked')){
                $( ".persons" ).prop( "checked", true );   
            }else{
                $( ".persons" ).prop( "checked", false );   
            }
        });
    });
</script>