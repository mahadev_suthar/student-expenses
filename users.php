<?php
include_once 'includes/header.php';
if (isset($_GET['success']) && $_GET['success'] == "1") {
    echo "<p style='color:green;'>Balance Added Successfully!</p>";
}
if (isset($_GET['message']) && $_GET['message'] == "1") {
    echo "<p style='color:green;'>User Updated Successfully!</p>";
}
if (isset($_GET['add']) && $_GET['add'] == "1") {
    echo "<p style='color:green;'>User Added Successfully!</p>";
}
$users = Query("Select * from `users` order by user_id desc");
?>
<h1>All Users</h1><br>
<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Amounts Paid</th>
            <th>Total Expenses</th>
            <th>Balance Left</th>
<!--            <th>Expense Details</th>-->
            <th colspan="2">Operations</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        while ($user = GetAssoc($users)) {
            
            $user_rec = Query("Select sum(amount) as total from `records` where user_id=".$user['user_id']);
            $totalExp = GetAssoc($user_rec);
            if ($i % 2 == "0")
                $class = "class='active'";
            else
                $class = "class='success'";
            ?>
            <tr <?= $class ?>>
                <td><?= $i ?></td>
                <td><?= $user['fullname'] ?></td>
                <td><?= $user['email'] ?></td>
                <td><?= formatNumber($user['amount_paid']) ?></td>
                <td><?= formatNumber($totalExp['total']) ?></td>
                <td><?= formatNumber(($user['amount_paid'] - $totalExp['total'])) ?></td>
                <td><a href="record_details.php?id=<?= $user['user_id'] ?>" class="btn btn-primary">Detail</a>
                <a href="edit_user.php?id=<?=$user['user_id'] ?>" class="btn btn-primary">Edit</a></td>
            </tr>
            <?php $i++;
        } ?>
    </tbody>
</table>
</div>
</div>
</div>
</body>
</html>
