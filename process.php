<?php

session_start();
include 'includes/inc_db_con.php';
//echo "<pre>";
//print_r($_POST);
//die;

if (isset($_GET['logout'])) {
    session_destroy();
    header("location: index.php");
    die;
}

if (isset($_POST) && !empty($_POST)) {
    
    if (isset($_POST['login']) && $_POST['login'] == "1") {
        if (($_POST['email'] == "admin@expenses.com") && ($_POST['password'] == "admin")) {
            $_SESSION['user']['email'] = $_POST['email'];
            $_SESSION['user']['password'] = $_POST['password'];
            header("location: add_expense.php");
            die;
        } else {
            header("location: index.php?error=1");
            die;
        }
    }

    if (isset($_POST['add_exp'])) {
        $details = $_POST['details'];
        $persons = $_POST['persons'];
        $total_per = count($persons);
        $per_one = $_POST['amount'] / $total_per;

        $d_res = Query_u("insert into `daily` (daily_amount,daily_details,daily_date) values ('" . $_POST['amount'] . "','" . $details . "','" . date("Y-m-d") . "')");
        if ($d_res) {
            foreach ($persons as $person) {
                Query("insert into `records` (user_id,daily_id,amount,details,exp_date) values ('" . $person . "','" . $d_res . "','" . $per_one . "','" . $details . "','" . date("Y-m-d") . "')");
            }
            header("location: all_expenses.php?success=1");
            die;
        }
    }
    
    if (isset($_POST['add_user'])) {
        $fullname = $_POST['fullname'];
        $email = $_POST['email'];
        $balance = $_POST['balance'];

        $d_res = Query_u("insert into `users` (fullname,email,amount_paid) values ('" . $fullname . "','" . $email . "','" . $balance . "')");
        if ($d_res) {
            header("location: users.php?add=1");
            die;
        }
    }

    if (isset($_POST['add_balance'])) {
        $amount = $_POST['amount'];
        $user = $_POST['user'];

        $u_res = Query("select amount_paid from `users` where user_id = '" . $user . "'");
        $usr = GetAssoc($u_res);
        if (isset($usr['amount_paid'])) {
            $new_balance = intval($usr['amount_paid'] + $amount);
            $i_res = Query("update `users` set amount_paid = '" . intval($new_balance) . "' where user_id = '" . $user . "'");
            if ($i_res) {
                header("location: users.php?success=1");
                die;
            }
        }
    }
}
?>