<?php
include_once 'includes/header.php';
?>
<h1>All Expenses</h1><br>
<table class="table" id="myTable">
    <thead>
        <tr>
            <th>#</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        $tot_daily = 0;
        $daily = Query("Select * from `daily` order by daily_id desc");
        $total_amount = Query("Select sum(amount_paid) as total_amount from `users`");
        $grand_total = GetAssoc($total_amount);
        while ($rec = GetAssoc($daily)) {
            if ($i % 2 == "0")
                $class = "class='active'";
            else
                $class = "class='success'";
            $tot_daily += $rec['daily_amount'];
            ?>
            <tr <?= $class ?>>
                <td><?= $i ?></td>
                <td><?= $rec['daily_date'] ?></td>
                <td><?= $rec['daily_amount'] ?></td>
                <td><?= $rec['daily_details'] ?></td>
            </tr>
            <?php
            $i++;
        }
        ?>

    </tbody>
</table>
<div class="daily_rec">
    <h4>Total Expended : <?= $tot_daily ?></h4>
    <h4>Total Remains  : <?= $grand_total['total_amount'] - $tot_daily ?></h4>
</div>
</div>
</div>
</div>
</body>
</html>
<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>
