<?php
include_once 'includes/header.php';
$users = Query("Select * from `users`");
?>
<h1>Add Balance</h1><br>
<form action="process.php" class="form-horizontal" method="post">
    <div class="form-group">
        <label for="amount" class="col-sm-2 control-label">Enter Amount</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="amount" id="amount" required="">
        </div>
    </div><br>

    <div class="form-group">
        <label for="details" class="col-sm-2 control-label">User</label>
        <div class="col-sm-4">
            <select name="user" class="form-control" required="">
                <option value="">Select</option>
                <?php
                while ($user = GetAssoc($users)) {
                    ?>
                    <option value="<?= $user['user_id'] ?>"><?php echo $user['fullname'] ?></option>
                <?php } ?>
            </select>
        </div>
    </div><br><br>


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="add_balance" class="btn btn-default">Submit</button>
        </div>
    </div>
</form>
</div>
</div>
</div>
</body>
</html>
