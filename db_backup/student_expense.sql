-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 27, 2019 at 03:18 AM
-- Server version: 10.1.41-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mahadevs_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `daily`
--

CREATE TABLE `daily` (
  `daily_id` int(11) NOT NULL,
  `daily_amount` int(11) NOT NULL,
  `daily_details` varchar(250) NOT NULL,
  `daily_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily`
--

INSERT INTO `daily` (`daily_id`, `daily_amount`, `daily_details`, `daily_date`) VALUES
(1, 800, 'Initial investment, oil, masala and others', '2019-01-08'),
(2, 165, 'Daal Roti Raita', '2019-01-09'),
(3, 150, 'Alu Mattar', '2019-01-10'),
(4, 320, 'Doodh Sodha and Shake', '2019-01-10'),
(5, 210, 'Chonra + Roti', '2019-01-11'),
(6, 520, 'Murgi, Bottle , Chae', '2019-01-12'),
(7, 210, 'Gatta plus Raita', '2019-01-14'),
(8, 230, 'Toori', '2019-01-14'),
(9, 167, 'Kuree Pakora', '2019-01-23'),
(10, 190, 'Gaijar mattaer palak', '2019-01-24'),
(11, 90, 'Bhujiya', '2019-02-01'),
(12, 80, 'dahi and chae', '2019-02-02'),
(13, 194, 'Eva oil', '2019-02-03'),
(14, 290, 'Alu anda grebi dahi with lalwani bros', '2019-02-04'),
(15, 330, 'Toorki, anda previous ajo le ayo to.', '2019-02-04'),
(16, 230, 'Gatta plus next day breakfast', '2019-02-05'),
(17, 160, 'Bhujya', '2019-02-06'),
(18, 170, 'Raito tamata chatni', '2019-02-11'),
(19, 360, 'Toori Raita', '2019-02-12'),
(20, 270, 'Alu mattar', '2019-02-13'),
(21, 1187, 'Oil, mirch masalo and gajjar palak', '2019-02-14'),
(22, 170, 'Bhujya raito', '2019-02-18'),
(23, 250, 'Toori', '2019-02-19'),
(24, 260, 'Mix', '2019-02-20'),
(25, 230, 'Bhujiya', '2019-02-21'),
(26, 535, 'Murgi with aneel Ashok', '2019-02-22'),
(27, 180, 'Baryal bhujya', '2019-02-25'),
(28, 150, 'Nashto', '2019-02-26'),
(29, 280, 'Toori', '2019-02-26'),
(30, 200, 'Daal masoor dinner', '2019-02-28'),
(31, 150, 'Nashto', '2019-02-28'),
(32, 90, 'Nashto', '2019-02-28'),
(33, 230, 'Mix bhaaji', '2019-02-28'),
(34, 130, 'Nashto', '2019-03-01'),
(35, 210, 'Chinese chanwar', '2019-03-01'),
(36, 85, 'Lobia', '2019-03-05'),
(37, 320, 'Toori raito', '2019-03-12'),
(38, 140, 'Nashto', '2019-03-13'),
(39, 120, 'Bhujya', '2019-03-18'),
(40, 150, 'Nashto', '2019-03-21'),
(41, 200, 'Toori', '2019-03-21'),
(42, 135, 'Lobia', '2019-03-26'),
(43, 170, 'Murgi ajeet treat', '2019-03-26'),
(44, 100, 'Nashto', '2019-03-27'),
(45, 1407, 'Oil, bhoonka,chhoka, toori', '2019-03-27'),
(46, 340, 'Breakfast, chanwar chinese', '2019-03-28'),
(47, 150, 'Nashto', '2019-03-29'),
(48, 310, 'Breakfast', '2019-03-30'),
(49, 190, 'Dinner paratha anda', '2019-03-31'),
(50, 250, 'Breakfast', '2019-03-31'),
(51, 160, 'Breakfast', '2019-04-01'),
(52, 220, 'Toori', '2019-04-01'),
(53, 150, 'Nashto', '2019-04-02'),
(54, 100, 'Daal', '2019-04-02'),
(55, 120, 'Breakfast ', '2019-04-04'),
(56, 190, 'Lobia', '2019-04-04'),
(57, 130, 'Breakfast', '2019-04-04'),
(58, 240, 'Alu bhindi ', '2019-04-09'),
(59, 160, 'Bhujya', '2019-04-10'),
(60, 170, 'Daal', '2019-04-15'),
(61, 140, 'Ando', '2019-04-15'),
(62, 130, 'Raito', '2019-04-16'),
(63, 170, 'Daal masoor', '2019-04-19'),
(64, 136, 'Alo', '2019-04-24'),
(65, 240, 'Toori', '2019-04-25'),
(66, 80, 'Rotinashto', '2019-04-26'),
(67, 250, 'Nashta', '2019-04-28'),
(68, 110, 'Daal masoor', '2019-04-30'),
(69, 108, 'Loon,jeero', '2019-05-09');

-- --------------------------------------------------------

--
-- Table structure for table `records`
--

CREATE TABLE `records` (
  `record_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `daily_id` int(11) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `details` text NOT NULL,
  `exp_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `records`
--

INSERT INTO `records` (`record_id`, `user_id`, `daily_id`, `amount`, `details`, `exp_date`) VALUES
(1, 1, 1, '160', 'Initial investment, oil, masala and others', '2019-01-12'),
(2, 2, 1, '160', 'Initial investment, oil, masala and others', '2019-01-12'),
(3, 3, 1, '160', 'Initial investment, oil, masala and others', '2019-01-12'),
(4, 4, 1, '160', 'Initial investment, oil, masala and others', '2019-01-12'),
(5, 5, 1, '160', 'Initial investment, oil, masala and others', '2019-01-12'),
(6, 1, 2, '33', 'Daal Roti Raita', '2019-01-12'),
(7, 2, 2, '33', 'Daal Roti Raita', '2019-01-12'),
(8, 3, 2, '33', 'Daal Roti Raita', '2019-01-12'),
(9, 4, 2, '33', 'Daal Roti Raita', '2019-01-12'),
(10, 5, 2, '33', 'Daal Roti Raita', '2019-01-12'),
(11, 1, 3, '30', 'Alu Mattar', '2019-01-12'),
(12, 2, 3, '30', 'Alu Mattar', '2019-01-12'),
(13, 3, 3, '30', 'Alu Mattar', '2019-01-12'),
(14, 4, 3, '30', 'Alu Mattar', '2019-01-12'),
(15, 5, 3, '30', 'Alu Mattar', '2019-01-12'),
(16, 1, 4, '64', 'Doodh Sodha and Shake', '2019-01-12'),
(17, 2, 4, '64', 'Doodh Sodha and Shake', '2019-01-12'),
(18, 3, 4, '64', 'Doodh Sodha and Shake', '2019-01-12'),
(19, 4, 4, '64', 'Doodh Sodha and Shake', '2019-01-12'),
(20, 5, 4, '64', 'Doodh Sodha and Shake', '2019-01-12'),
(21, 1, 5, '42', 'Chonra + Roti', '2019-01-12'),
(22, 2, 5, '42', 'Chonra + Roti', '2019-01-12'),
(23, 3, 5, '42', 'Chonra + Roti', '2019-01-12'),
(24, 4, 5, '42', 'Chonra + Roti', '2019-01-12'),
(25, 5, 5, '42', 'Chonra + Roti', '2019-01-12'),
(26, 1, 6, '104', 'Murgi, Bottle , Chae', '2019-01-12'),
(27, 2, 6, '104', 'Murgi, Bottle , Chae', '2019-01-12'),
(28, 3, 6, '104', 'Murgi, Bottle , Chae', '2019-01-12'),
(29, 4, 6, '104', 'Murgi, Bottle , Chae', '2019-01-12'),
(30, 5, 6, '104', 'Murgi, Bottle , Chae', '2019-01-12'),
(31, 1, 7, '42', 'Gatta plus Raita', '2019-01-14'),
(32, 2, 7, '42', 'Gatta plus Raita', '2019-01-14'),
(33, 3, 7, '42', 'Gatta plus Raita', '2019-01-14'),
(34, 4, 7, '42', 'Gatta plus Raita', '2019-01-14'),
(35, 5, 7, '42', 'Gatta plus Raita', '2019-01-14'),
(36, 1, 8, '46', 'Toori', '2019-01-14'),
(37, 2, 8, '46', 'Toori', '2019-01-14'),
(38, 3, 8, '46', 'Toori', '2019-01-14'),
(39, 4, 8, '46', 'Toori', '2019-01-14'),
(40, 5, 8, '46', 'Toori', '2019-01-14'),
(41, 1, 9, '41.75', 'Kuree Pakora', '2019-01-23'),
(42, 3, 9, '41.75', 'Kuree Pakora', '2019-01-23'),
(43, 4, 9, '41.75', 'Kuree Pakora', '2019-01-23'),
(44, 5, 9, '41.75', 'Kuree Pakora', '2019-01-23'),
(45, 1, 10, '47.5', 'Gaijar mattaer palak', '2019-01-24'),
(46, 3, 10, '47.5', 'Gaijar mattaer palak', '2019-01-24'),
(47, 4, 10, '47.5', 'Gaijar mattaer palak', '2019-01-24'),
(48, 5, 10, '47.5', 'Gaijar mattaer palak', '2019-01-24'),
(49, 1, 11, '30', 'Bhujiya', '2019-02-01'),
(50, 2, 11, '30', 'Bhujiya', '2019-02-01'),
(51, 3, 11, '30', 'Bhujiya', '2019-02-01'),
(52, 1, 12, '26.666666666667', 'dahi and chae', '2019-02-02'),
(53, 2, 12, '26.666666666667', 'dahi and chae', '2019-02-02'),
(54, 3, 12, '26.666666666667', 'dahi and chae', '2019-02-02'),
(55, 1, 13, '48.5', 'Eva oil', '2019-02-03'),
(56, 2, 13, '48.5', 'Eva oil', '2019-02-03'),
(57, 3, 13, '48.5', 'Eva oil', '2019-02-03'),
(58, 4, 13, '48.5', 'Eva oil', '2019-02-03'),
(59, 1, 14, '96.666666666667', 'Alu anda grebi dahi with lalwani bros', '2019-02-04'),
(60, 3, 14, '96.666666666667', 'Alu anda grebi dahi with lalwani bros', '2019-02-04'),
(61, 4, 14, '96.666666666667', 'Alu anda grebi dahi with lalwani bros', '2019-02-04'),
(62, 1, 15, '82.5', 'Toorki, anda previous ajo le ayo to.', '2019-02-04'),
(63, 2, 15, '82.5', 'Toorki, anda previous ajo le ayo to.', '2019-02-04'),
(64, 3, 15, '82.5', 'Toorki, anda previous ajo le ayo to.', '2019-02-04'),
(65, 4, 15, '82.5', 'Toorki, anda previous ajo le ayo to.', '2019-02-04'),
(66, 1, 16, '76.666666666667', 'Gatta plus next day breakfast', '2019-02-05'),
(67, 2, 16, '76.666666666667', 'Gatta plus next day breakfast', '2019-02-05'),
(68, 3, 16, '76.666666666667', 'Gatta plus next day breakfast', '2019-02-05'),
(69, 1, 17, '40', 'Bhujya', '2019-02-06'),
(70, 2, 17, '40', 'Bhujya', '2019-02-06'),
(71, 3, 17, '40', 'Bhujya', '2019-02-06'),
(72, 4, 17, '40', 'Bhujya', '2019-02-06'),
(73, 1, 18, '42.5', 'Raito tamata chatni', '2019-02-11'),
(74, 2, 18, '42.5', 'Raito tamata chatni', '2019-02-11'),
(75, 3, 18, '42.5', 'Raito tamata chatni', '2019-02-11'),
(76, 4, 18, '42.5', 'Raito tamata chatni', '2019-02-11'),
(77, 1, 19, '90', 'Toori Raita', '2019-02-12'),
(78, 2, 19, '90', 'Toori Raita', '2019-02-12'),
(79, 3, 19, '90', 'Toori Raita', '2019-02-12'),
(80, 4, 19, '90', 'Toori Raita', '2019-02-12'),
(81, 1, 20, '67.5', 'Alu mattar', '2019-02-13'),
(82, 2, 20, '67.5', 'Alu mattar', '2019-02-13'),
(83, 3, 20, '67.5', 'Alu mattar', '2019-02-13'),
(84, 4, 20, '67.5', 'Alu mattar', '2019-02-13'),
(85, 1, 21, '296.75', 'Oil, mirch masalo and gajjar palak', '2019-02-14'),
(86, 2, 21, '296.75', 'Oil, mirch masalo and gajjar palak', '2019-02-14'),
(87, 3, 21, '296.75', 'Oil, mirch masalo and gajjar palak', '2019-02-14'),
(88, 4, 21, '296.75', 'Oil, mirch masalo and gajjar palak', '2019-02-14'),
(89, 1, 22, '42.5', 'Bhujya raito', '2019-02-18'),
(90, 2, 22, '42.5', 'Bhujya raito', '2019-02-18'),
(91, 3, 22, '42.5', 'Bhujya raito', '2019-02-18'),
(92, 4, 22, '42.5', 'Bhujya raito', '2019-02-18'),
(93, 1, 23, '62.5', 'Toori', '2019-02-19'),
(94, 2, 23, '62.5', 'Toori', '2019-02-19'),
(95, 3, 23, '62.5', 'Toori', '2019-02-19'),
(96, 4, 23, '62.5', 'Toori', '2019-02-19'),
(97, 1, 24, '65', 'Mix', '2019-02-20'),
(98, 2, 24, '65', 'Mix', '2019-02-20'),
(99, 3, 24, '65', 'Mix', '2019-02-20'),
(100, 4, 24, '65', 'Mix', '2019-02-20'),
(101, 1, 25, '57.5', 'Bhujiya', '2019-02-21'),
(102, 2, 25, '57.5', 'Bhujiya', '2019-02-21'),
(103, 3, 25, '57.5', 'Bhujiya', '2019-02-21'),
(104, 4, 25, '57.5', 'Bhujiya', '2019-02-21'),
(105, 1, 26, '133.75', 'Murgi with aneel Ashok', '2019-02-22'),
(106, 2, 26, '133.75', 'Murgi with aneel Ashok', '2019-02-22'),
(107, 3, 26, '133.75', 'Murgi with aneel Ashok', '2019-02-22'),
(108, 4, 26, '133.75', 'Murgi with aneel Ashok', '2019-02-22'),
(109, 1, 27, '45', 'Baryal bhujya', '2019-02-25'),
(110, 2, 27, '45', 'Baryal bhujya', '2019-02-25'),
(111, 3, 27, '45', 'Baryal bhujya', '2019-02-25'),
(112, 4, 27, '45', 'Baryal bhujya', '2019-02-25'),
(113, 1, 28, '37.5', 'Nashto', '2019-02-26'),
(114, 2, 28, '37.5', 'Nashto', '2019-02-26'),
(115, 3, 28, '37.5', 'Nashto', '2019-02-26'),
(116, 4, 28, '37.5', 'Nashto', '2019-02-26'),
(117, 1, 29, '70', 'Toori', '2019-02-26'),
(118, 2, 29, '70', 'Toori', '2019-02-26'),
(119, 3, 29, '70', 'Toori', '2019-02-26'),
(120, 4, 29, '70', 'Toori', '2019-02-26'),
(121, 1, 30, '50', 'Daal masoor dinner', '2019-02-28'),
(122, 2, 30, '50', 'Daal masoor dinner', '2019-02-28'),
(123, 3, 30, '50', 'Daal masoor dinner', '2019-02-28'),
(124, 4, 30, '50', 'Daal masoor dinner', '2019-02-28'),
(125, 1, 31, '37.5', 'Nashto', '2019-02-28'),
(126, 2, 31, '37.5', 'Nashto', '2019-02-28'),
(127, 3, 31, '37.5', 'Nashto', '2019-02-28'),
(128, 4, 31, '37.5', 'Nashto', '2019-02-28'),
(129, 1, 32, '30', 'Nashto', '2019-02-28'),
(130, 2, 32, '30', 'Nashto', '2019-02-28'),
(131, 3, 32, '30', 'Nashto', '2019-02-28'),
(132, 1, 33, '57.5', 'Mix bhaaji', '2019-02-28'),
(133, 2, 33, '57.5', 'Mix bhaaji', '2019-02-28'),
(134, 3, 33, '57.5', 'Mix bhaaji', '2019-02-28'),
(135, 4, 33, '57.5', 'Mix bhaaji', '2019-02-28'),
(136, 1, 34, '32.5', 'Nashto', '2019-03-01'),
(137, 2, 34, '32.5', 'Nashto', '2019-03-01'),
(138, 3, 34, '32.5', 'Nashto', '2019-03-01'),
(139, 5, 34, '32.5', 'Nashto', '2019-03-01'),
(140, 1, 35, '70', 'Chinese chanwar', '2019-03-01'),
(141, 2, 35, '70', 'Chinese chanwar', '2019-03-01'),
(142, 3, 35, '70', 'Chinese chanwar', '2019-03-01'),
(143, 1, 36, '28.333333333333', 'Lobia', '2019-03-05'),
(144, 3, 36, '28.333333333333', 'Lobia', '2019-03-05'),
(145, 4, 36, '28.333333333333', 'Lobia', '2019-03-05'),
(146, 1, 37, '64', 'Toori raito', '2019-03-12'),
(147, 2, 37, '64', 'Toori raito', '2019-03-12'),
(148, 3, 37, '64', 'Toori raito', '2019-03-12'),
(149, 4, 37, '64', 'Toori raito', '2019-03-12'),
(150, 5, 37, '64', 'Toori raito', '2019-03-12'),
(151, 1, 38, '35', 'Nashto', '2019-03-13'),
(152, 2, 38, '35', 'Nashto', '2019-03-13'),
(153, 3, 38, '35', 'Nashto', '2019-03-13'),
(154, 5, 38, '35', 'Nashto', '2019-03-13'),
(155, 1, 39, '30', 'Bhujya', '2019-03-18'),
(156, 2, 39, '30', 'Bhujya', '2019-03-18'),
(157, 3, 39, '30', 'Bhujya', '2019-03-18'),
(158, 5, 39, '30', 'Bhujya', '2019-03-18'),
(159, 1, 40, '37.5', 'Nashto', '2019-03-21'),
(160, 2, 40, '37.5', 'Nashto', '2019-03-21'),
(161, 3, 40, '37.5', 'Nashto', '2019-03-21'),
(162, 5, 40, '37.5', 'Nashto', '2019-03-21'),
(163, 1, 41, '66.666666666667', 'Toori', '2019-03-21'),
(164, 2, 41, '66.666666666667', 'Toori', '2019-03-21'),
(165, 5, 41, '66.666666666667', 'Toori', '2019-03-21'),
(166, 1, 42, '33.75', 'Lobia', '2019-03-26'),
(167, 2, 42, '33.75', 'Lobia', '2019-03-26'),
(168, 3, 42, '33.75', 'Lobia', '2019-03-26'),
(169, 5, 42, '33.75', 'Lobia', '2019-03-26'),
(170, 1, 43, '42.5', 'Murgi ajeet treat', '2019-03-26'),
(171, 2, 43, '42.5', 'Murgi ajeet treat', '2019-03-26'),
(172, 3, 43, '42.5', 'Murgi ajeet treat', '2019-03-26'),
(173, 5, 43, '42.5', 'Murgi ajeet treat', '2019-03-26'),
(174, 1, 44, '25', 'Nashto', '2019-03-27'),
(175, 2, 44, '25', 'Nashto', '2019-03-27'),
(176, 3, 44, '25', 'Nashto', '2019-03-27'),
(177, 5, 44, '25', 'Nashto', '2019-03-27'),
(178, 1, 45, '351.75', 'Oil, bhoonka,chhoka, toori', '2019-03-27'),
(179, 2, 45, '351.75', 'Oil, bhoonka,chhoka, toori', '2019-03-27'),
(180, 3, 45, '351.75', 'Oil, bhoonka,chhoka, toori', '2019-03-27'),
(181, 5, 45, '351.75', 'Oil, bhoonka,chhoka, toori', '2019-03-27'),
(182, 1, 46, '85', 'Breakfast, chanwar chinese', '2019-03-28'),
(183, 2, 46, '85', 'Breakfast, chanwar chinese', '2019-03-28'),
(184, 3, 46, '85', 'Breakfast, chanwar chinese', '2019-03-28'),
(185, 5, 46, '85', 'Breakfast, chanwar chinese', '2019-03-28'),
(186, 1, 47, '37.5', 'Nashto', '2019-03-29'),
(187, 2, 47, '37.5', 'Nashto', '2019-03-29'),
(188, 3, 47, '37.5', 'Nashto', '2019-03-29'),
(189, 5, 47, '37.5', 'Nashto', '2019-03-29'),
(190, 1, 48, '77.5', 'Breakfast', '2019-03-30'),
(191, 2, 48, '77.5', 'Breakfast', '2019-03-30'),
(192, 3, 48, '77.5', 'Breakfast', '2019-03-30'),
(193, 5, 48, '77.5', 'Breakfast', '2019-03-30'),
(194, 1, 49, '47.5', 'Dinner paratha anda', '2019-03-31'),
(195, 2, 49, '47.5', 'Dinner paratha anda', '2019-03-31'),
(196, 3, 49, '47.5', 'Dinner paratha anda', '2019-03-31'),
(197, 5, 49, '47.5', 'Dinner paratha anda', '2019-03-31'),
(198, 1, 50, '62.5', 'Breakfast', '2019-03-31'),
(199, 2, 50, '62.5', 'Breakfast', '2019-03-31'),
(200, 3, 50, '62.5', 'Breakfast', '2019-03-31'),
(201, 5, 50, '62.5', 'Breakfast', '2019-03-31'),
(202, 1, 51, '53.333333333333', 'Breakfast', '2019-04-01'),
(203, 2, 51, '53.333333333333', 'Breakfast', '2019-04-01'),
(204, 3, 51, '53.333333333333', 'Breakfast', '2019-04-01'),
(205, 1, 52, '55', 'Toori', '2019-04-01'),
(206, 2, 52, '55', 'Toori', '2019-04-01'),
(207, 3, 52, '55', 'Toori', '2019-04-01'),
(208, 5, 52, '55', 'Toori', '2019-04-01'),
(209, 1, 53, '37.5', 'Nashto', '2019-04-02'),
(210, 2, 53, '37.5', 'Nashto', '2019-04-02'),
(211, 3, 53, '37.5', 'Nashto', '2019-04-02'),
(212, 5, 53, '37.5', 'Nashto', '2019-04-02'),
(213, 1, 54, '25', 'Daal', '2019-04-02'),
(214, 2, 54, '25', 'Daal', '2019-04-02'),
(215, 3, 54, '25', 'Daal', '2019-04-02'),
(216, 5, 54, '25', 'Daal', '2019-04-02'),
(217, 1, 55, '30', 'Breakfast ', '2019-04-04'),
(218, 2, 55, '30', 'Breakfast ', '2019-04-04'),
(219, 3, 55, '30', 'Breakfast ', '2019-04-04'),
(220, 5, 55, '30', 'Breakfast ', '2019-04-04'),
(221, 2, 56, '63.333333333333', 'Lobia', '2019-04-04'),
(222, 3, 56, '63.333333333333', 'Lobia', '2019-04-04'),
(223, 5, 56, '63.333333333333', 'Lobia', '2019-04-04'),
(224, 1, 57, '32.5', 'Breakfast', '2019-04-04'),
(225, 2, 57, '32.5', 'Breakfast', '2019-04-04'),
(226, 3, 57, '32.5', 'Breakfast', '2019-04-04'),
(227, 5, 57, '32.5', 'Breakfast', '2019-04-04'),
(228, 1, 58, '60', 'Alu bhindi ', '2019-04-09'),
(229, 2, 58, '60', 'Alu bhindi ', '2019-04-09'),
(230, 3, 58, '60', 'Alu bhindi ', '2019-04-09'),
(231, 5, 58, '60', 'Alu bhindi ', '2019-04-09'),
(232, 1, 59, '40', 'Bhujya', '2019-04-10'),
(233, 2, 59, '40', 'Bhujya', '2019-04-10'),
(234, 3, 59, '40', 'Bhujya', '2019-04-10'),
(235, 5, 59, '40', 'Bhujya', '2019-04-10'),
(236, 1, 60, '42.5', 'Daal', '2019-04-15'),
(237, 2, 60, '42.5', 'Daal', '2019-04-15'),
(238, 3, 60, '42.5', 'Daal', '2019-04-15'),
(239, 5, 60, '42.5', 'Daal', '2019-04-15'),
(240, 1, 61, '35', 'Ando', '2019-04-15'),
(241, 2, 61, '35', 'Ando', '2019-04-15'),
(242, 3, 61, '35', 'Ando', '2019-04-15'),
(243, 5, 61, '35', 'Ando', '2019-04-15'),
(244, 1, 62, '32.5', 'Raito', '2019-04-16'),
(245, 2, 62, '32.5', 'Raito', '2019-04-16'),
(246, 3, 62, '32.5', 'Raito', '2019-04-16'),
(247, 5, 62, '32.5', 'Raito', '2019-04-16'),
(248, 1, 63, '42.5', 'Daal masoor', '2019-04-19'),
(249, 2, 63, '42.5', 'Daal masoor', '2019-04-19'),
(250, 3, 63, '42.5', 'Daal masoor', '2019-04-19'),
(251, 5, 63, '42.5', 'Daal masoor', '2019-04-19'),
(252, 1, 64, '34', 'Alo', '2019-04-24'),
(253, 2, 64, '34', 'Alo', '2019-04-24'),
(254, 3, 64, '34', 'Alo', '2019-04-24'),
(255, 5, 64, '34', 'Alo', '2019-04-24'),
(256, 1, 65, '60', 'Toori', '2019-04-25'),
(257, 2, 65, '60', 'Toori', '2019-04-25'),
(258, 3, 65, '60', 'Toori', '2019-04-25'),
(259, 5, 65, '60', 'Toori', '2019-04-25'),
(260, 1, 66, '20', 'Rotinashto', '2019-04-26'),
(261, 2, 66, '20', 'Rotinashto', '2019-04-26'),
(262, 3, 66, '20', 'Rotinashto', '2019-04-26'),
(263, 5, 66, '20', 'Rotinashto', '2019-04-26'),
(264, 1, 67, '62.5', 'Nashta', '2019-04-28'),
(265, 2, 67, '62.5', 'Nashta', '2019-04-28'),
(266, 3, 67, '62.5', 'Nashta', '2019-04-28'),
(267, 5, 67, '62.5', 'Nashta', '2019-04-28'),
(268, 1, 68, '36.666666666667', 'Daal masoor', '2019-04-30'),
(269, 2, 68, '36.666666666667', 'Daal masoor', '2019-04-30'),
(270, 3, 68, '36.666666666667', 'Daal masoor', '2019-04-30'),
(271, 1, 69, '27', 'Loon,jeero', '2019-05-09'),
(272, 2, 69, '27', 'Loon,jeero', '2019-05-09'),
(273, 3, 69, '27', 'Loon,jeero', '2019-05-09'),
(274, 5, 69, '27', 'Loon,jeero', '2019-05-09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `fullname` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `amount_paid` bigint(20) NOT NULL,
  `status` enum('active','deactive') NOT NULL DEFAULT 'deactive'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `fullname`, `email`, `amount_paid`, `status`) VALUES
(1, 'Maha Dev', 'mahadev.suthar84@gmail.com', 4500, 'active'),
(2, 'Ajeet Paal', 'ajeet@paal.com', 3870, 'active'),
(3, 'Kamlesh Kumar', 'kamlesh@kumar.com', 4310, 'active'),
(4, 'Ajeet Lalwani', 'ajeet@lalwani.com', 2430, 'deactive'),
(5, 'Dasrat Kumar', 'dasrat@kumar.com', 2500, 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daily`
--
ALTER TABLE `daily`
  ADD PRIMARY KEY (`daily_id`);

--
-- Indexes for table `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`record_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daily`
--
ALTER TABLE `daily`
  MODIFY `daily_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `records`
--
ALTER TABLE `records`
  MODIFY `record_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=275;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
